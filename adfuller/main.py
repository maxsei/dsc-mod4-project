import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller, acf, pacf
import json
from numba import jit
import gc
import sys


def output_fuller_stats(lag):
    # print("../logdifflag/loglags/09_loglagof%03d.csv" % lag)
    df = pd.read_csv("../logdifflag/loglags/09_loglagof%03d.csv" % lag)
    # df_ts = df.iloc[:, 8:].replace([np.inf, -np.inf], np.nan).dropna(axis=1)
    df_ts = df.iloc[:, 8+lag:]
    del df
    gc.collect()

    output = {}
    output["adfuller_results"] = []

    for i in range(len(df_ts)):
    # for i in range(100):
        # print(df_ts.iloc[i].dropna())
        adf_dict = {}
        adf_dict["SizeRank"] = i + 1
        adf = None
        try:
            adf = adfuller(df_ts.iloc[i].dropna(), autolag="AIC")
        except:
            output["adfuller_results"].append(adf_dict)
            continue
        for j, k in enumerate(
            ["TestStatistic", "PValue", "LagsUsed", "NumberofObservationsUsed"]
        ):
            adf_dict[k] = adf[j]
        adf_dict["threshholds"] = adf[-2]
        output["adfuller_results"].append(adf_dict)
        # if i % 1000 == 0:
        #     print("iteration: %s\t lag: %s"%(i, lag))
    # print(len(output["adfuller_results"]))
    with open("adf_results/result%03d.json" % lag, "w+") as f:
        f.truncate()
        f.write(json.dumps(output, indent=4))


output_fuller_stats(int(sys.argv[1]))
