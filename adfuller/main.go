package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"sync"
)

func main() {
	lags := 24

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for i := 1; i <= lags; i++ {
			wg.Add(1)
			go func(i int) {
				cmd := exec.Command("python", "main.py", fmt.Sprintf("%d", i))
				stderr, err := cmd.StderrPipe()
				if err != nil {
					log.Fatal(err)
				}
				if err := cmd.Start(); err != nil {
					log.Fatal(err)
				}

				slurp, _ := ioutil.ReadAll(stderr)
				if len(slurp) == 0 {
					fmt.Printf("%s\n", slurp)
				}
				if err := cmd.Wait(); err != nil {
					log.Fatal(err)
				}
				wg.Done()
			}(i)
		}
		wg.Done()
	}()
	wg.Wait()
	cmd := exec.Command("vlc", "-L", "that-was-easy.mkv")
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
