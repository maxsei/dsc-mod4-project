package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"sync"
)

func main() {
	f, err := os.Open("df_2012.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	r := csv.NewReader(f)

	rawData, err := r.ReadAll()
	if err != nil {
		panic(err)
	}

	// read in the data splitting the data on the time series data
	var header []string
	data := make([][]float64, len(rawData)-1)
	for i, v := range rawData {
		if i == 0 {
			header = v
			continue
		}
		var row []float64
		for j := 7; j < len(v); j++ {
			fl, err := strconv.ParseFloat(rawData[i][j], 64)
			if err != nil {
				row = append(row, math.NaN())
				continue
			}
			row = append(row, fl)
		}
		data[i-1] = row
	}
	fmt.Println(head(data))

	// make log function
	// myLog := func(x float64) float64 {
	// 	if math.IsNaN(x) {
	// 		return 0
	// 	}
	// 	if x == 0 {
	// 		return 0
	// 	}
	// 	if x < 0 {
	// 		return math.Log(-1 * x)
	// 	}
	// 	return math.Log(x)
	// }

	// create lag data between 1-24
	var wg sync.WaitGroup
	for lag := 1; lag <= 24; lag++ {
		// tranform data first diffing and then loging
		logged := apply(math.Log, data)
		logDiffData := diff(lag, logged)
		output := make([][]string, len(rawData))
		// append header, original "meta" data, and tranformed numerical data
		// where necessary
		for i, v := range rawData {
			if i == 0 {
				output[i] = header
				continue
			}
			output[i] = rawData[i]
			for j := 7; j < len(v); j++ {
				if math.IsNaN(logDiffData[i-1][j-7]) {
					output[i][j] = "NaN"
					continue
				}
				output[i][j] = strconv.FormatFloat(logDiffData[i-1][j-7], 'f', 8, 64)
			}
		}
		// write data concurrently to file
		wg.Add(1)
		go func(outData [][]string, i int) {
			of, err := os.OpenFile(fmt.Sprintf("loglags/09_loglagof%03d.csv", i), os.O_RDWR|os.O_CREATE, 0755)
			if err != nil {
				panic(err)
			}
			defer of.Close()
			if err := of.Truncate(0); err != nil {
				panic(err)
			}
			toCSV(of, outData)
			fmt.Println(i)
			wg.Done()
		}(output, lag)
	}
	wg.Wait()
}

func toCSV(w io.Writer, data [][]string) {
	csvw := csv.NewWriter(w)
	for _, record := range data {
		if err := csvw.Write(record); err != nil {
			log.Fatalln("error writing record to csv:", err)
		}
	}
}

func apply(transform func(float64) float64, data [][]float64) [][]float64 {
	result := make([][]float64, len(data))
	for i := 0; i < len(data); i++ {
		resultRow := make([]float64, len(data[i]))
		for j := 0; j < len(data[i]); j++ {
			resultRow[j] = transform(data[i][j])
		}
		result[i] = resultRow
	}
	return result
}

func diff(lag int, data [][]float64) [][]float64 {
	result := make([][]float64, len(data))
	for i := 0; i < len(data); i++ {
		resultRow := make([]float64, len(data[i]))
		for j := 0; j < len(data[i]); j++ {
			if j < lag {
				resultRow[j] = math.NaN()
				continue
			}
			resultRow[j] = data[i][j] - data[i][j-lag]
		}
		result[i] = resultRow
	}
	return result
}

func pass(interface{}) {
	return
}

func head(data [][]float64) string {
	result := ""
	for i := 0; i < 5 && i < len(data); i++ {
		v := data[i]
		if len(v) > 8 {
			result += fmt.Sprintf("%d | [%s ... %s]\n", i, rowStr(v[:4]), rowStr(v[len(v)-5:]))
			continue
		}
		result += fmt.Sprintf("%d | %v\n", i, rowStr(v))
	}
	return result
}

func rowStr(row []float64) string {
	result := ""
	for i := range row {
		if i == len(row)-1 {
			result += fmt.Sprintf("%0.4f", row[i])
			break
		}
		result += fmt.Sprintf("%0.4f, ", row[i])
	}
	return result
}
