import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller, acf, pacf
import json
from numba import jit
import gc

def output_fuller_stats(lag):
    print("loglags/09_loglagof%03d.csv" % lag)
    df = pd.read_csv("loglags/09_loglagof%03d.csv" % lag)
    df_ts = df.iloc[:, 8:]
    # del df
    # gc.collect()
    # df_ts = df_ts.iloc[:, lag:].bfill()
    # print(pd.unique(df_ts.dtypes))

    output = {}
    output["adfuller_results"] = []

    n = range(lag, len(df_ts))
    # n = range(lag, 10)
    for i in n:
        # print(df_ts.iloc[i].dropna())
        adf_dict = {}
        adf_dict["SizeRank"] = i + 1
        adf = None
        try:
            adf = adfuller(df_ts.iloc[i].dropna())
        except:
            output["adfuller_results"].append(adf_dict)
            continue

        for j, k in enumerate(
            ["TestStatistic", "PValue", "LagsUsed", "NumberofObservationsUsed"]
        ):
            adf_dict[k] = j
        output["adfuller_results"].append(adf_dict)

    with open("adf_results/result%03d.json" % lag, "w+") as f:
        f.truncate()
        f.write(json.dumps(output, indent=4))

