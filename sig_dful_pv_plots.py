import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import json
from statsmodels.tsa.stattools import adfuller, acf, pacf

pd.set_option("display.max_rows", 120)
plt.style.use("ggplot")


def get_pvalue_df(lag):
    significant_pvalues = []
    dful_results = None
    with open("adfuller/adf_results/result%03d.json" % lag) as f:
        dful_results = json.load(f)
    for obs in dful_results["adfuller_results"]:
        try:
            if obs["PValue"] > 0.05:
                continue
            significant_pvalues.append(obs)
        except:
            continue
    return pd.DataFrame(significant_pvalues, dtype=np.float64)


def replace_strwnan(s):
    try:
        result = float(s)
        return result
    except:
        return np.nan


# error here
# significant_lag_log_dfn_ts = significant_lag_log_dfn_ts.astype(np.float64)

lags = 24
# fig, ax = plt.subplots(lags, figsize=(10, 8 * lags))
# plt.subplots_adjust(hspace=0.5)
print(0)
for lag in range(1, lags + 1):
    plt.figure(figsize=(10, 8))
    pvalue_df = get_pvalue_df(lag)
    lag_log_dfn = pd.read_csv(
        "logdifflag/loglags/09_loglagof%03d.csv" % lag, low_memory=False
    )
    significant_lag_log_dfn = lag_log_dfn.loc[pvalue_df["SizeRank"]]
    significant_lag_log_dfn_ts = lag_log_dfn.iloc[:, 8 + lag :].T
    significant_lag_log_dfn_ts = significant_lag_log_dfn_ts.applymap(replace_strwnan)
    nobs = significant_lag_log_dfn_ts.shape[1]
    plt.title("logged data with a diff lag of %d    Num Obs %d" % (lag, nobs))
    #     plt.plot(significant_lag_log_dfn_ts, alpha=1000/nobs, color="green", label="individual zipcodes")
    #     plt.plot(significant_lag_log_dfn_ts.mean(axis=1), color="pink", lw=5, label="mean zipcode log difference")
    plt.plot(
        significant_lag_log_dfn_ts,
        "r-",
        alpha=1 / nobs ** 0.5,
        color="green",
        rasterized=True,
    )
    plt.plot(
        significant_lag_log_dfn_ts.mean(axis=1),
        "r-",
        color="pink",
        lw=5,
        rasterized=True,
    )
    plt.xticks(significant_lag_log_dfn_ts.index[::3], rotation=45)
    # plt.setp(plt.xaxis.get_majorticklabels(), rotation=45)
    plt.ylabel("Logged Difference of t-%d" % lag)
    plt.xlabel("Time")
    plt.savefig("sig_dful_pv_plots/%03d.png" % lag, bbox_inches="tight")
    plt.close()

    print(lag)
    # ax[lag-1].legend(loc="upper left")
    #     display(lag_log_dfn.shape, lag_log_dfn.head())

# plt.show()
# plt.savefig("sig_dful_pv_plots.pdf", bbox_inches="tight", pil_kwargs="jpeg")
