# /usr/bin/env python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def main():
    df = pd.read_csv("zillow_data.csv")
    print("loaded data")
    plt.style.use("ggplot")
    # for i in range(df.shape[0]):
    num_plots = 50;
    # fig, ax = plt.subplots(df.shape[0], 1, figsize=(12.59, 7.87 * df.shape[0]))
    fig, ax = plt.subplots(num_plots, 1, figsize=(12.59, 7.87 * num_plots))
    print("created subplots")
    # fig, ax = plt.subplots(10, 1, figsize=(12.59, 7.87 * 10))
    plt.tight_layout()
    for i in range(num_plots):
        print(i)
        first_region_info = df[df.iloc[i].index[:7]].iloc[i]
        first_dates = df[df.iloc[i].index[7:]].iloc[i]
        # print(np.array(first_dates.values == np.na))
        ax[i].set_title(str(first_region_info))
        ax[i].plot(first_dates)
        # ax[i].set_xticks(first_dates.index[::10], rotation=90)
        ax[i].set_xticks(first_dates.index[::10])
        ax[i].set_xlabel("Date")
        ax[i].set_ylabel("Average Cost of a House?")
        # ax[i].close()
    # fig.show()
    plt.savefig("first_50.pdf")


def increment_subplots(i):
    # if i % 2 ==
    pass


if __name__ == "__main__":
    main()
